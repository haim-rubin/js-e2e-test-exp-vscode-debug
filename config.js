exports.config = {
    framework: 'jasmine',
    onPrepare: () => {
        const SpecReporter = require('jasmine-spec-reporter');
        // add jasmine spec reporter
        jasmine.getEnv().addReporter(new SpecReporter({displayStacktrace: 'all'}));
    },
    jasmineNodeOpts: {
        print: () => {}
    },
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['specs/calculator.js']
}