const title = 'Super Calculator'

const selectValue = ( value ) => {
    const select = element.all(by.tagName('select')).first()
    const option = element(by.css(option[value='${value}']));
    select.click()
    option.click()  
}

const actions = {
    '+':'ADDITION',
    '/':'DIVISION',
    '%':'MODULO',
    '*':'MULTIPLICATION',
    '-':'SUBTRACTION'
}

describe(title, () => {
    
    browser.get('http://juliemr.github.io/protractor-demo/')
    const select = element.all(by.tagName('select')).first()
    const option = element(by.css('option[value="MULTIPLICATION"]'));
    select.click()
    option.click()
    
    it('Verify page header', (done) =>{
        expect(browser.getTitle()).toEqual(title)
        done()
    })

    const firstValue = 10
    const secondValue = 5
    const go = element(by.css('.btn'))
    const first = element(by.css('[ng-model=first]'))
    const second = element(by.css('[ng-model=second]'))
    const result = element.all(by.css('.ng-binding')).first()
    
    const calc = () => {
        first.clear()
        second.clear()

        first.sendKeys(firstValue)
        second.sendKeys(secondValue)

        go.click()
    }

    it(`Calculate ${firstValue} + ${secondValue}`, (done) => {   
        selectValue(actions['+'])
        calc()
        result.getText().then(value => {
        expect(parseInt(value)).toEqual(firstValue + secondValue)
        done()
        })    
    });

    it(`Calculate ${firstValue} * ${secondValue}`, (done) => {   
        selectValue(actions['*'])
        calc()
        result.getText().then(value => {
        expect(parseInt(value)).toEqual(firstValue * secondValue)
        done()
        })    
    });

    it(`Calculate ${firstValue} / ${secondValue}`, (done) => {   
        selectValue(actions['/'])
        calc()
        result.getText().then(value => {
        expect(parseInt(value)).toEqual(firstValue / secondValue)
        done()
        })    
    });

    it(`Calculate ${firstValue} % ${secondValue}`, (done) => {   
        selectValue(actions['%'])
        calc()
        result.getText().then(value => {
        expect(parseInt(value)).toEqual(firstValue % secondValue)
        done()
        })    
    });
    
    it(`Calculate ${firstValue} - ${secondValue}`, (done) => {   
        selectValue(actions['-'])
        calc()
        result.getText().then(value => {
        expect(parseInt(value)).toEqual(firstValue - secondValue)
        done()
        })    
    });
    //browser.pause();
  
});